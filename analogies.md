# Analogies

## First-class Functions

- Golang is like Python or JavaScript, in that it has a keyword for defining functions.
  - But it spells it `func` rather than `lambda` or `function`.
- Golang is like JavaScript or even Python, in that it offers true [lexical closures](https://en.wikipedia.org/wiki/Closure_(computer_programming))
- Golang is like Java or Python, in that it does not perform [tail-call optimization](https://en.wikipedia.org/wiki/Tail_call)

## Variables

- Golang is like most modern languages, in that variables are always initialized to some value.
  But it is unusual in that the "zero" value of many types is most useful.
  Design your structures similarly.
  - A "zero" string is simply the empty string.
  - A "zero" slice is simply a slice with size of 0.
  - A "zero" mutex is simply a mutex in unlocked state.
  - Only maps and channels (and perhaps slices) need an explicit "make" -- https://go.dev/doc/effective_go#allocation_make

## Syntax

- Golang is unique, in that the capitalization of functions, field names, and module-scope variables in a module determines whether they are exported or not. 
  - Trivia: Functions with a name of `init()` are executed in each package before the main package's `main()` and are useful for initializing package-scope variables. I have yet to find a need for this functionality.
- Golang is like Python, in that statements are usually not delimited by semicolons `;`.
- Golang is like TypeScript, in that declarations have the type after the variable name. Also, types are built from right-to-left. At first, this may be utterly disconcerting: `var topTen [10]string` or `words := [3]string{"boom", "chuggala", "boom"}`
- Golang is like C/Java in that it has a three-statement for loop. 
  - No parens around the condition
  - Mandatory curl braces `{body}`
- Golang is different from other languages with switch statements, in that every `case` has an implicit `break`.
  - Golang is almost unique, in that switch cases can be arbitrary boolean-valued predicates.
- Golang is different, in that backquotes ``` ` ``` wrap literal strings (i.e. no backslash escaping)
- Golang is like most modern languages, in that comments immediately before a function are extracted as documentation, e.g. `go doc http`
- Golang is unique, in that `p->f` (`i.e., (*p).f`) is spelled `p.f`
- Golang was the first major language to have a code-formatter built into its toolchain.
  Use it!

## Runtime

- Golang is like Java, in that it has Garbage Collection.
  - (It's also like Java in that the GC has gotten better and better over the years.)
- Golang is like C, in that it is compiled rather than interpreted.
  - (But it does have a runtime performing GC.)
- Golang is unlike C, in that all variables are initialized.
  - Golang is different, in that it strives for the "zero" value to be useful whenever possible.
- Golang is like Lisp, in that references may be `nil` rather than `null`.
- Golang is like Rust, in that the core language is designed with safety as a primary requirement – but specific code can be annotated as "unsafe" and bypass those protections.
  - It is also like Rust, in that you can have a long and successful career without ever using those unsafe features.

## Primitive values

- Golang is like JavaScript, in that functions are first-class values.
- Golang is like C or Java, in that arrays have a fixed size and cannot be resized in-place.
  - Use "Array Slices" whenever possible (and it's almost always possible).
- Golang is like C/Java, in that indexing past the end of a string or array results in panic.
- Golang is like Rust, in that one is encouraged to be explicit about integer size (e.g. int8, int16, int32, int64, etc).
- Golang is unusual, in that it doesn't perform automatic type casting (e.g. it is an error to add an int16 and an int32)
  - Go does have a consistent convention that `T(x)` casts `x` to a value of type `T`. 
  - Loss of precision when down-casting is fine, but overflow or underflow causes a compile error.
- Golang is unlike most languages, in that untyped numeric constants have greater precision at compile time than any runtime primitive types.
- Golang is like Fortran, in that complex numbers are standard primitives.
  - (Note that complex64 encapsulates a pair of float32's, etc.)
- Golang is like Java, in that instances of the string type are immutable

## Types

- Golang is like TypeScript, in that it is strongly-typed but frequently deduces types without the programming having to do anything explicit.
- Golang is like TypeScript, in that interfaces are duck-typed -- if a type happens to have the required methods, then it is considered to implement the interface.
  - No explicit "implements" is necessary (or even desirable).
- Golang is like JavaScript, in that key/value maps (a/k/a dictionaries) are a core part of the language.
  - Golang is like Perl, in that missing map values simply evaluate to the "zero" value for the relevant type.
- Golang is finally, finally like most modern languages, in that it offers Generics.

## Tuples

- Golang is like most languages other than Java and C/C++, in that it allows tuple assignment. (`result, err = f(x)` is a near-universal idiom.)
- Golang is like Java, in that all function arguments are passed by value -- but references may be passed so that the function may modify their underlying values. (Array slices are internally mostly just a reference.)

## Concurrency

- Golang is like Java, in that it has threads, and it's up to the programmer to use mutexes to protect variables accessed from multiple threads. But Golang is like Erlang, in that message-passing is the preferred method for inter-thread communication.

## Control Flow

- Golang is like Java in that it has a finally concept, but it *only* applies at function scope and is spelled `defer`.