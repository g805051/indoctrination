# Messaging Trust Principles

> So, how do we want to *feel* when we're about to deploy to production? Relaxed or anxious?

DORA Metrics -- https://cloud.google.com/blog/products/devops-sre/
- Deployment Frequency -- How often an organization successfully releases to production
- Lead Time for Changes -- The amount of time it takes a commit to get into production
- Change Failure Rate -- The percentage of deployments causing a failure in production
- Time to Restore Service -- How long it takes an organization to recover from a failure in production
- Reliability -- Availability, latency, performance, and scalability

```mermaid
mindmap
 Messaging-Trust
  All internal comms are protobuf messages delivered over gRPC.
   REST only for clients we don't control.
   Go context -- both deadlines and tracing identifiers -- propagates over gRPC.
   A separate, semantic-versioned, git project defines protobuf structures.
  Service Mesh -- gRPC is mediated through a linkerd "sidecar" colocated on each k8s pod.
   linkerd does service-discovery.
   linkerd does internal load-balancing.
   linkerd does mTLS and zero-trust.
   linkerd makes pretty traffic graphs.
  Define testable microservices
   Downstream microservices should not directly interact with each other.
   Let an Orchestrator microservice coordinate everything.
   A microservice should wrap exactly one resource -- a computation resource, a set of database tables, etc.
  Kubernetes, a/k/a "k8s"
   Everything is a Docker image.
    All images are created by Gitlab pipelines.
    Spin up an environment on your laptop.
   Everything is deployed through Terraform.
   Environments are defined in an "infrastructure" git repo.
    Environments, as well, should be cattle.
```

[^Semver]: https://semver.org/

### What are protobufs and gRPC?

- Messaging trust defines protobufs and services in the `.proto` files within https://gitlab.com/syniverse1/messaging-trust/mt-protos/-/tree/develop/pb[^ProtobufMakefile]
- Protobufs are a compact and backwards-compatible on-the-wire data format, along with client libraries which offer strong type checking.
  ```proto
  message MDNClassificationRequest {
    string mdn = 1;
  }

  message MDNClassificationResponse {
    string error = 1;
    string mdn = 2;
    string classification = 3;
    float  likelihood = 4;
  }
  ```
- gRPC is a service definition -- a collection of methods, each of which accepts a protobuf of one type and returns a protobuf of another type.
  ```proto
  service MDNClassifier {
    rpc MDNClassify (MDNClassificationRequest) returns (MDNClassificationResponse) {}
  }
  ```
- Protobufs are backward-compatible -- fields may be added at any time[^FieldRenaming]. 
  - `mt-protos` is tagged in accordance with https://semver.org/.
  - But for best results, upgrade the server before upgrading the client.

[^ProtobufMakefile]: https://gitlab.com/syniverse1/messaging-trust/mt-protos/-/blob/develop/Makefile creates Golang, Python, and Java code for our service definitions.
[^FieldRenaming]: Fun fact: one can actually *rename* fields without any problem -- as long as the semantics remain the same. Client and server don't have to agree on field names -- just their field numbers.

### Why use protobufs and gRPC?

- The standard reasons
  - Protobufs are compact, computationally cheap, language-neutral, and supported for many popular languages.[^ProtobufLangs]
  - gRPC builds on protobufs and is also supported by most modern languages.[^gRPClangs]
  - gRPC allows use of frameworks like linkerd, yielding Service Discovery, Zero-Trust Security, and Load Balancing without additional effort.
- Even more important
  - Deliberately planning your microservices in terms of the request protobufs they accept (e.g. `MDNClassificationRequest`) and the protobufs they return (e.g. `MDNClassificationResponse`) is key to a robust and testable system.
    - Services whose responses are purely determined by their inputs are cacheable.
    - Services whose responses are purely determined by their inputs are testable.
    - Services whose responses are purely determined by their inputs are scaleable.
    - Services that query downstream services are not purely determined by their inputs.
    - Services that query databases are not purely determined by their inputs.
  - Simple rule: pass to each service all the information it needs.
    - Simple exception: a microservice may violate this principle if it fully commits.
      A microservice which does *nothing* but wrap a database doesn't have any business logic to test and so is fine.
- This isn't just at the system architecture level.
  - Go is such a powerful language because data and behavior are passed, rather than the SOLID OO orientation of Java.
  - Channels are safer, more testable, and usually more efficient than mutexes.

[^ProtobufLangs]: C++, C#, Java, Kotlin, Objective-C, PHP, Python, Ruby, Dart, Go
[^gRPClangs]: C#/.NET, C++, Dart, Go, Java, Kotlin, Node, Objective-C, PHP, Python, Ruby

## Design Principles

```mermaid
mindmap
Principles
  Testability is paramount. Plan the testing before planning the coding.
    Only homogenous components are testable.
     Avoid mutexes: channels separate Producer from Consumer, allowing each to be tested independently.
     Passed functions separate behavior of different types, allowing each to be tested independently.
    Velocity relies upon trust; trust relies upon automated testing and deployment.
  Build complex systems upon testable components.
    Encapsulating state within an object is far inferior to eliminating state entirely.
    Usually, we simply need to pass behavior, e.g. functions.
  Respect conventions
    Pass context down the stack
    Return error values
    Communicate with channels
    Avoid leakage with \`defer\`
```

## Preparation

### Caveats

- All of the this was tested using Ubuntu running under WSL2, but should also work under OSX or an actual Linux install.
- Most of this was developed using the free-and-excellent VSCode, though some was done using nothing more than vim.[^TextEditor]
  But nothing should be different if you happen to use some other IDE, e.g. JetBrain's GoLand.
  - No code or process should be dependent upon a particular IDE; nothing IDE-specific should be checked into common Git repos.

[^TextEditor]: It is actually a good exercise to occasionally use nothing but a text-editor, so your fingers can learn some of the steps that IDEs normally do automatically. (And you will be a happier person if you take the time to learn how to use https://marketplace.visualstudio.com/items?itemName=vscodevim.vim)

### Git and Markdown

- Git isn't a just a collaboration tool ... it is also a *development* tool, a *management* tool, a *documentation* tool.
  Get used to doing basic tasks from the command-line, so when you want someone else on your team to follow-along you can just cut-and-paste the CLI command.
  Everything should be in Git, and if that isn't yet possible for your project (e.g. Route53 configuration isn't yet IAC) then you should be loudly documenting that fact about your project.
  - Always err on the side of committing more frequently.  Have you added or enhanced a single unit test and gotten it to pass: time to commit to git. If you perform a single refactoring: time to commit.  Doing anything else: you probably should be doing one of the previous two steps -- but we'll discuss that more later.
- Everything should be in Git, including infrastructure. Terraform allows this: IAM privileges, VM requirements, deployment versions all belong in an "infrastructure" repo.
- Documentation which does not co-evolve with the code it describes is dead documentation.
    [Markdown](https://www.markdownguide.org/) allows for well-formatted documentation to be defined in plain text files.
    [Mermaid](https://mermaid.js.org/intro/) extends Markdown to allow you to create excellent diagrams using plain text.
    [VSCode](https://code.visualstudio.com/Docs/languages/markdown), especially with [plugins](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-mermaid) supports both, so you can edit your code and documentation in the same tool.
    Markdown and Mermaid support is built into GitHub and GitLab, e.g. https://handbook.gitlab.com/docs/markdown-guide/.
    - For all but the largest projects, just putting this documentation into `README.md` suffices.
      Concise up-to-date information helps a project, whereas fancy-but-out-of-date documentation is dangerous.
    - Merge Requests reviewers should think about the proposed changes to documentation no less than they think about the actual code changes.
    - This document is written in Markdown + Mermaid and hosted in GitLab.
      Please submit Merge Requests!
  - Git repos should each contain one type of information[^CoevolvingDocumentation]. For example, for the Messaging Trust project, we have:
    - Configuration -- [mt-infrastructure](https://gitlab.com/syniverse1/messaging-trust/mt-infrastructure) --- this defines our production and non-production environments
      - Terraform rules
      - Per-environment microservice versions and configurations
      - Static data
    - Messaging -- [mt-protos](https://gitlab.com/syniverse1/messaging-trust/mt-protos) -- This defines the gRPC calls supporte by each microservice.
      It is vital that this repository truly comply with https://semver.org/.
      Note that each microservice will explicitly state what version it uses.
      As long as servers are never "behind" the clients which call them, we should be OK.
    - Endpoint Service -- [mt-resolver](https://gitlab.com/syniverse1/messaging-trust/mt-resolver) -- this defines an externally-visible service (HTTP and/or gRPC). It should *not* directly talk to any database or other resource.  Instead, it uses the microservices defined in the following repos
    - Microservices -- [mt-nlp-classifier](https://gitlab.com/syniverse1/messaging-trust/mt-nlp-classifier), [mt-mdn-classifier](https://gitlab.com/syniverse1/messaging-trust/mt-mdn-classifier), etc.
      - Wraps a _single_ resource, e.g. a DynamoDB table or a ML model.
        Wrapping should be *complete*: no code, other than the microservice, should have access to the wrapped resource.
      - Should only expose a gRPC interface (not http or REST)
      - Until there is a compelling argument for making our architecture more complex, avoid having microservices call other microservices.
        Instead, allow the endpoint service to orchestrate the flow of data [^UtilityMicroServices].
    - Non-production -- [mt-bucket](https://gitlab.com/syniverse1/messaging-trust/mt-bucket)
      - Integration tests
      - Sundry utilities.
- Continuous improvement isn't just a catchword -- it's a vital part of a healthy team.
  For example, this document is hosted in a GitLab repo.
  As you read through it, think about how it could be made better ... and then send Merge Requests to improve it for the next reader.
  (The best code is the result of an active and lively Merge Request review process...the same is true of everything else.)

[^CoevolvingDocumentation]: Of course, each repo should contain a `README.md` file documenting that repo!
[^UtilityMicroServices]:  Of course, there is no additional complexity in allowing microservices to call "fire-and-forget" microservices, e.g. logging and/or tracing.

### Automation

- Install and use https://github.com/casey/just.
  - [ ] `just fmt` should reformat the source code in a project.
  - [ ] `just test` should run all unit tests in a project.
    (It's fine if `just` in turns calls `make` when scripting is easier to understand that way.)
  - [ ] `just plan` and `just apply` should be all that's necessary to deploy any project from its infrastructure git project.


## Golang, Part 1

### Modules

- `go.mod` defines the structure of the module.
  - Every Golang git repo defines exactly one module.
    The module name must match its repo if you want to ever be able to reference it from another project.
    `module gitlab.com/syniverse1/messaging-trust/mt-protos.git` is a perfectly-acceptable module directive.
  - Go is released approximately semi-annually, has strong backwards-compatiblity-guarantees, and only supports the two most recent releases. So `go 1.22` is the proper module directive for new code, until `go 1.23` is released.
    - Upgrading an existing system to a new version of Go should be no more complicated than updating the file and re-running your automated tests.
      If you do not trust the above statement, then that means that you don't trust that your code-review process detects code changes insufficiently covered by automated tests.
- `go.sum` should also be stored in Git -- it specifies the *exact* version of every dependency used by your code.[^GoModVendor]
  The ensures that builds are essentially reproducible, and so should be stored in Git.

[^GoModVendor] We also use the `go mod vendor` feature, which ensures that builds don't even need a network connection -- but that can wait until we talk about gRPC.

### Our first program

- Some observations about [45bce161bed1a633a2eb9fc75f150ade2b4c6b36](../45bce161bed1a633a2eb9fc75f150ade2b4c6b36/pkg/counter/counter_test.go)
  - Library packages[^Packages] go within `pkg/`.
    (Eventually, actual entry points will go in `cmd/`, but "Hello World" is so 1970's.)
  - `CreateCounter` is a function
    - `CreateCounter` has no parameters.
      (This makes sense...counters start at 0.)
    - `CreateCounter()` asks (using the `()` syntax) the higher-order function `CreateCounter` to perform its computations, and those computations return a function which itself returns an `int` every time it is called.
  - From the `pkg/counter` directory: `go test .` will test and `go fmt .` will apply standard code formatting.
    - Soon enough we'll create `just` recipes to invoke this across the entire project.
- Now let's add another test.
  - If you add the test but don't change the code, you'll see an error:
    ```bash
    mgflax@SYN-PF39HNRL:~/mt/indoctrination/pkg/counter (develop) $ go test .
    --- FAIL: Test1 (0.00s)
        counter_test.go:25: Second time: Expected:  1  but got  0
    FAIL
    FAIL    gitlab.com/g805051/indoctrination.git/pkg/counter       0.002s
    FAIL
    ```
  - Also note that the first test in [57057197f8de7f1fc942fd7114b19d29fc1a85f5](../57057197f8de7f1fc942fd7114b19d29fc1a85f5/pkg/counter/counter_test.go) differs from the second test.
    - The first time we wrote `expected := 0` because we're telling Go that we're introducing a *new* variable (and that Go should deduce its type from the value on the right).
      But when we wrote `expected = 1` we were telling Go to *update* an existing variable.
      However, this ugliness is the result of a deeper ugliness: the `expected` and `actual` variables really should have lexical scope limited to the test for which they're assigned.
    - We *could* fix this by manually creating two lexical sub-scopes:
      ```go
        {
          expected := 0
          actual := counter1()
          if expected != actual {
            t.Error("First Time: Expected: ", expected, " but got ", actual)
          }
        }

        {
          expected := 1
          actual := counter1()
          if expected != actual {
            t.Error("Second time: Expected: ", expected, " but got ", actual)
          }
        }
      ```
    - But Go allows syntactic sugar accomplishing the same effect with far less typing:
      ```go
      if expected, actual := 0, counter1(); expected != actual {
        t.Error("First Time: Expected: ", expected, " but got ", actual)
      }

      if expected, actual := 1, counter1(); expected != actual {
        t.Error("Second Time: Expected: ", expected, " but got ", actual)
      }
      ```
    - And now we can actually get both tests to pass -- [97dc73a2900a0e7cad3874cf5e9e40f2ef375c7c](../97dc73a2900a0e7cad3874cf5e9e40f2ef375c7c/pkg/counter/counter_test.go)
      - Note that Java allows lambda functions to *read* variables (and only `final` variables) in their surrounding scope, but is incapable of supporting true "lexical closures".
        (Whereas Go and JavaScript do.)
      - It might not be possible to overstate the impact of this single feature.
        In any case, now is a good time to step back and play around with this new feature.
        For example, create two counters and see that they are truly independent of each other -- [2bd73bec7b308809690b8c0ba10766e7cb2d1905](../2bd73bec7b308809690b8c0ba10766e7cb2d1905/pkg/counter/counter_test.go)

[^Packages]: https://go.dev/doc/effective_go#package-names

---

### Loops, etc

- The repeated code in [97dc73a2900a0e7cad3874cf5e9e40f2ef375c7c](../97dc73a2900a0e7cad3874cf5e9e40f2ef375c7c/pkg/counter/counter_test.go) should bother you, especially since we want many more scenarios to be tested.
  - In such cases, the creators of Go prefer that you use core language constructs yourself, rather than looking for a library facilitating table-driven testing.
    (This is a general principle in Go: a little code is preferable to a little library.)
- [3d27a996a621ee82772c632b6c83796e1fe32073](../3d27a996a621ee82772c632b6c83796e1fe32073/pkg/counter/counter_test.go) illustrates multiple core Go features.
  - Even for complex variables like the `sequence` variable in the example, determines the type from whatever is on the right hand side of the `:=`
    - There are also ways to explicitly declare variables before assigning them non-empty values, but you should use them only when necessary.
  - Slices are declared with a simple `[]`.
    (The difference between Go slices and Go arrays is very important, but not yet.)
  - Structures are defined by just listing fields and their types.
    - This is better than having to declared interfaces or classes all the time.
    - Go does make it easy to define named structures and interfaces, but doesn't require you to do so.
  - The syntax is utterly reversed to C or Java:
    - Brackets `[` and `]` are *before* the array or slice; types are *after* the variable name.
    - You'll get used to it faster than you expect, but it will certainly annoy you at first.
  - The `range` keyword provides an iterator along the slice of triples we just created.
  - The iterator supplies a pair: (index, value).
  - We don't care about the index into the test array, so we assign it to `_`.
    - If we had assigned it to a named variable, "just in case we need it", the code would not compile: it is a compile-time error to define a variable and not use it.
      (This is a feature and not a bug.)
  - Note how convenient it is: the ability to assign a comma-delimited of values to a comma-delimited list of variables.
    - This ability has been in Python since the very start.
    - This ability isn't yet in JDK 21, and is likely to never be available to Java users.
    - (We'll see later on that this ability to return a "tuple" radically simplifies error handling.)

### Functions are values

- In Go, numbers are values.
  In Go, functions are also values.
  They do different things (e.g. functions perform computations whereas numbers are comparable), but both may be stored in structs.
- Note that we need *zero* additional syntax in order to encapsulate the lexical closure: [1e5065443d1bd5d2cb6722f2aa10ea9af159b3b9](./1e5065443d1bd5d2cb6722f2aa10ea9af159b3b9/pkg/counter/counter_test.go
)
  - And we can return a structure containing more than one function field, if we like: [83c2791252d1c0d30fd3984d79d2f98a4bec6466](../83c2791252d1c0d30fd3984d79d2f98a4bec6466/pkg/counter/counter_test.go)
  - But please don't create "Get" and "Set" functions unless there's a specific need.
    (Hint: there is no need.)
- Observation: lexical closures provide a *stronger* privacy than Java, without even needing any kind of `private` keyword.

### Recap so far

- Structures contain values
- Functions are values
- Tuple assignment is useful
- Define using `:=`. [^var]
  Reassign using `=`.
  Prefer definition to reassignment.

[^var]: Golang also offers other ways to declare variables: the `var` and `const` keywords.
        `const` is useful, but you rarely will need `var`.

### References

- https://go-proverbs.github.io/
- https://go.dev/doc/effective_go
- https://agilemanifesto.org/principles.html
- https://cloud.google.com/blog/products/devops-sre/using-the-four-keys-to-measure-your-devops-performance
- https://www.youtube.com/watch?v=llaUBH5oayw&list=PLwLLcwQlnXByqD3a13UPeT4SMhc3rdZ8q

## Golang, Part 2

### https://pkg.go.dev/std

- Get to know the standard Golang packages.
  They are well-designed, and usually encourage your code in good directions.
  - https://pkg.go.dev/context
    - Context propagates over gRPC
      - Cancellation/timeouts
      - Tracing information[^TraceModule]
    - In general, every function should accept a context object as its first parameter, and pass it down (perhaps decorated with more restrictive deadlines, or with a span_id) the stack.
  - https://pkg.go.dev/strings
  - https://pkg.go.dev/io
  - https://pkg.go.dev/fmt

[^TraceModule]: https://gitlab.com/syniverse1/messaging-trust/mt-protos/-/tree/develop/pkg/tracing

### Additional aphorisms

- Types are mostly sugar
  - *Anything* which happens to offer a `Read(p []byte) (n int, err error)` method *is* an `io.Reader`.
    No `implements` keyword is required, though interfaces may extend other interfaces.[^EmbeddedInterfaces]
- Eschew heterogeneous data structures.
  - Writing instances of `interface{}` to a channel is a useful idiom when you want to send empty messages for signalling.
  - But declaring types to be `interface{}` in order to avoid type checking is an anti-pattern.
- Eschew heterogeneous control structures.
  - Each function should perform one type of activity.
    - A function which invokes an external service should not also contain any business logic.
    - A function which contains business logic should not also be controlling concurrency.
  - Collaboration amongst functions mediated by:
    - The call stack (i.e., functions can, of course, call other functions).
    - Passing messages over channels.
      "Don't communicate by sharing memory; share memory by communicating." [^ShareByCommunicating]
    - Functions as values.
      - Higher-order functions themselves return functions.
      - Flexible functions accept functions as parameters.

[^ShareByCommunicating]: https://go.dev/blog/codelab-share
[^EmbeddedInterfaces]: https://go.dev/doc/effective_go#embedding

---

## Diagrams

### Orchestrator

```mermaid
graph LR
 start((Start!)) --> working
 working((Waiting...)) ---> |some computation has completed| check
 working ---> |a timer has fired| check
 working ---> |no more time| check
 working ---> |just started| check
 sendRequests --> working
 check[Apply Policies] ==> |Not ready to decide| working
 check ===> |Block| done1((Done!))
 check ===> |Accept| done2((Done!))
 check ==> |remote computations to initiate| sendRequests[Send Requests]
 linkStyle 5 stroke-width:6px;
 linkStyle 6 stroke:yellow,stroke-width:6px;
 linkStyle 7 stroke:red,stroke-width:6px;
 linkStyle 8 stroke:green,stroke-width:6px;
 linkStyle 9 stroke:purple,stroke-width:6px;
```

---
---
---
