# Cheat sheet

## Operators

- Assignment operators, e.g. `+=`, are supported -- but are statements rather than expressions.
  - OK: `i++`, `i--`, `i *= 10` 
  - NOT OK: `--i`, `if (i-- > 0)` 
- `&&` and `||` short-circuit.
- `...` for variadic parameters

## Array, slices, references, etc.

- Arrays are not references.
  Arrays are a fixed-size region of memory containing a number of values.
  (Their memory layout is that of a repetitive struct.)
- Arrays must always be created with a concrete size, e.g. `[4]int` or `[n]int`.
  Syntactic sugar may do the counting for you, e.g. `[...]int{2,3,5,7,11}`
- Unless otherwise initialized, arrays are always created containing explicit "zeroish" values
- Generally, prefer slices to arrays
  - Passing an array by value causes Go to create a new array and copy all of the data to it.
  - Slices *contain* a reference to an underlying array.
  - Slices maintain enough information to know where to start and stop within the array.
- Slices may be based on existing array by specifying the half-open range of indices to include, e.g. `arr[start:end]` will include all indices satisfying `start <= idx < end`. This is the One True Way[^CountingFromZero].
  - (Exactly the same as Java's `.substring()` method.)
  - If not specified, start defaults to 0. If not specified, end defaults to the length of the array.
    - So `arr[:]` is an array slice pointing to the entire `arr` array, and `arr[1:]` omits the first element of arr.
- Slices may also be created directly using `make()`, in which case an anonymous underlying array will be created.
  - When creating a slice directly, you may specify the length and (optionally) total capacity.
  - For example, when creating a slice from an existing data structure, it is more efficient to set the capacity of the slice to the size of the original data structure.
- `cap(mySlice)` returns the current capacity of a slice.
  - `s[0:len(s)]` (or `s[:len(s)]`) is essentially identical to `s`. 
  - BUT `s[0:cap(s)]` will likely be larger than `s`.
- Slices of course can be built upon other slices.
  This has GC ramifications.
- Modifying the data within a slice actually changes the data in the underlying array. Race conditions are likely when different slices backed by the same underlying array are modified in different go threads.
  - In all cases, a reference to the underlying array continues unless it is replaced by a reference to a different underlying array.
- To increase the size of a slice beyond the range allowed by the underlying array, something must: (1) allocate a larger array (double the size is typical), (2) copy the data from the old array to the new array, and (3) update the pointer in the slice.
  - (Not normally used) The builtin `copy()` function does the second step.
  - (Use this one) The builtin `append()` function does all three steps, and also determines when it is necessary.
    - vararg-style expansion, e.g. `newSlice := append(slice1, slice2...)` concatenates.
   - Note that two slices created from the same array will share underlying data.
     But if one of the slices is replaced by the result of `append()` (or the manual equivalent), then this might or might not be true -- depending upon whether a new underlying array was created.
- All of the usual slice methods (e.g. `len()`) give sane values for nil slices.


## package, import

- Package names should be single words: no camelCase or snake_case.
- `./go.mod` declares a module and the packages/versions upon which it depends. https://go.dev/doc/modules/gomod-ref 
  - For those familiar with Node.js -- package.json:package-lock.json :: go.mod:go.sum
- All packages except main are libraries; func main() is the entrypoint.

## Concurrency

- `go` -- Execute a function in a separate cheap thread (or something which behaves as a separate cheap thread).
  - `GOMAXPROCS` (typically set to the number of cores dedicated to the server) determines how many OS-system threads may actually be used by all goroutines.
- `chan T` -- The type of a bounded thread-safe intra-process queue containing messages of type T. 
  - `chan <- T` is a write-only-channel type (useful for passing to writing functions).
  - `<- chan T` is a receive-only-channel type (common for passing to consumers).
  - Default size of a channel is 0, but any constant size may be specified.
    - With a zero-size channel, the writer blocks until the reader asks for the message.
  - `c, ok <- val` or `c <- val` blocks until the channel has space and then appends `val` to the channel.
  - `<- c` blocks until the channel contains a message is available and then evaluates to that message and removes it from the channel. 
  - `close(c)` causes the `ok` result to be false -- once the prior messages are consumed!
    - `for v := range c` takes care of all of this in the expected way.
  - `c2 <- <- c1`, which is really `c2 <- (<- c1)`, reads a message from `c1` and writes it to `c2` (blocking as necessary, of course)
- `select` -- Blocks until any one of a number of blocking statements is ready to execute.
  - `case` for each blockable expression
  - `time.After(1 * time.Second)` blocks for one second and then emits the time, functioning as a timeout.
  - `default`, if it is included in the `select`, is executed if all cases are currently blocked.

## Function

- `func` -- Declares both named and anonymous functions.
  - Functions (and methods) are first-class values.
  - Golang supports true closures (but take the usual precautions when a single closure is concurrently called from multiple goRoutines).
    - Conversely, a function may return a reference to a local variable and the GC will keep the original variable as long as the reference persists[^StackVsHeap].
    (The predefined function `new(T)` is basically syntactic sugar for this.)
  - *Methods* specify the subject object in parens, e.g. `func (t textClassifier) Classify(ctx context.Context, request Request) (response Response, err error) {`. Because Golang is strictly pass-by-value, mutating methods need to be func (t *T) rather than func (t T) ... but for consistency and usually efficiency it's common for all methods for an object to be func (t *T).

Methods receiving pointers may receive nil, but this should be documented and the code should handle nil appropriately.

No special constructor syntax.

if, else

Condition doesn't need parens; curly braces mandatory

Conditions can be preceded by assignments, e.g. if x := f(); x == 0 {}

for, range

"for (i = 0; i < n; i++) {}" is spelled for i=0; i < n; i++ {}

"while (someCondition) {}" is spelled for someCondition {}

"while (true) {}" is spelled for {}

"for x in xs {}" is spelled for idx, x := range xs {} or for _, x := range xs {} (if you don't care about the index)

range makes sense for lists, maps, and channels.

Q: Why isn't something like https://github.com/serge-hulne/go_iter  in the Go std library?

for i, val := range myArray

switch, case, default, fallthrough

Different from C/Java -- every case has implicit break. Instead, use fallthrough when you really want to.

defer

Execute the function until the surrounding function exits. Similar to Java finally, and useful for releasing resources external to the function.

break, continue, return, goto

Usual meanings, except Golang has a "naked return" feature which is only suitable for small functions.

var, const, iota

Declares variables (and tuples of variables) with implicit or explicit types, e.g. var i, j int = 1, 2

Alternatively, := declares variables with implicit types

iota is an APL-ish auto-incrementing counter useful for defining enum-like types.

map

map[string]int is the type of a map from string to int, and make(map[string]int) actually constructs an empty map of that type

Maps, as well as arrays, are not thread-safe unless protected by Mutex or RWMutex

struct, interface

struct introduces struct literals and struct types.

"Struct tags" -- https://go.dev/wiki/Well-known-struct-tags 

Structs may contained an unnamed member, in which case methods methods on the unnamed member are presented as methods on the struct.

interface introduces a set of function signatures.

Interfaces are "Duck typed": if a type happens satisfies all of the signatures in the set, then it implicitly is implements the interface

type -- Declares new types, e.g. type Vertex struct {X int; Y int;} or type AdderFunc func(int, int) int

Type assertions: v, ok = x.(T)

rune -- synonym for int32, used to store Unicode code points.

for idx, runeVal := range "Hello, world!" {}

Golang is natively unicode-compliant

Source code is always interpreted as UTF-8.

string is a readonly slice of bytes -- perhaps invalid Unicode, and that's ok.

 
[^CountingFromZero]: https://www.cs.utexas.edu/users/EWD/transcriptions/EWD08xx/EWD831.html

[^StackVsHeap]: As a consequence, the decision as to whether a variable is on the stack or heap requires static code analysis.

make() (special function, rather than actually a keyword)

Initializes maps, channels, and slices
