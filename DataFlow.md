```mermaid
graph TB
  MMS ---> |text attachments| Text
  MMS ---> |image attachments| ImageNormalizer(Image conversion and scaling)
  MMS ---> |image attachments| URLRecognizer
  MMS ---> |image attachments| QRScanner
  URLRecognizer --> |bounding boxes| URLScanner
  ImageNormalizer ---> |resized jpgs| ImageClassifier
  MMS --> |smil attachments| SmilParser --> Text
  ImageClassifier --> |likelihoods| Policy
  MMS --> |image attachments| OCR --> Text
  Text[Text Message] 
  Text --> |message text| Regex(RegexClassifier)
  Text ------> |message text| Regex4(PerSender, PerReceiver, PerSenderReceiver Regex)
  Regex --> |likelihoods| Policy
  Text --> |sender| Regex4
  Text --> |receiver| Regex4
  Regex4 --> |likelihoods| Policy
  Text --> |message text| NLP(NLPClassifier)
  NLP --> |likelihoods| Policy
  Text --> |sender| MDN(MDNClassifier)
  MDN --> |likelihood| Policy{{Policy}}
  Text --> |message text| xURL(xURL)
  URLScanner --> xURL
  QRScanner --> xURL
  xURL --> |urls| Domain(domain extraction)
  Domain --> |domains| TopDomains 
  TopDomains --> |nearby domains| SpoofLogic
  SpoofLogic --> |isTop,ifSpoof| DomainML
  Text ------> |sender| UrlOverride
  Text ------> |receiver| UrlOverride
  UrlOverride --> |block/allow/abstain| Policy
  xURL --> |urls| UrlOverride(URL Overrides)
  Domain --> |domains| DomainOverride(Domain overrides)
  DomainOverride --> |block/abstain| Policy
  Domain --> |domains| DomainHistory(whois history)
  DomainHistory --> |domain,registration,expiration| DomainML(URL Model)
  Redirect --> |redirectionCount| DomainML
  xURL --> |urls| Redirect(Redirect history)
  xURL --> |urls| DomainML
  SMS --> Text
  DomainML -->|likelihoods| Policy
  classDef future fill:#f33;
  classDef input fill:#3f3;
  classDef existing fill:#77f;
  class Regex2,Regex3,Regex4,OCR,SmilParser,DomainHistory,Domain,DomainOverride,Redirect,DomainML,ImageClassifier,UrlOverride future;
  class URLRecognizer,URLScanner,QRScanner,TopDomains,SpoofLogic future;
  class MMS,SMS input;
  class Regex,MDN,NLP,xURL,Text,Policy,ImageNormalizer existing;
  ```

---

- This diagram is intended to indicate the dependencies amongst the inputs and outputs of the microservices comprising ML-R.
  - It is not intended to show actual connectivity amongst components.
  - Instead, assume that the Orchestrator service takes care of all of this.
  - Similarly, this purpose of this diagram is to show *functional* components. Non-functional components, e.g. caches, should be considered as implementation details.
- Future components are colored in RED.
- If we want "url suffix substitution", that would make the above diagram more complex, e.g. 
  - `Macy's: It's on! Earn rewards faster during Star Money Bonus Days. Shop now:  t.macys.com/Ko3N3uQk   Details:  t.macys.com/N-Qiy530   Txt STOP=End. ` vs
  - `Macy's: It's on! Earn rewards faster during Star Money Bonus Days. Shop now:  t.macys.com/zzz   Details:  t.macys.com/zzz   Txt STOP=End. `