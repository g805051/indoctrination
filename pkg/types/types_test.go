package types

import (
	"testing"
)

type Type1 struct {
	FirstField  string
	SecondField string
}

type Type2 struct {
	FirstField  string
	SecondField string
}

type Type3 struct {
	FirstField  string
	SecondField string
	ThirdField  string
}

func fieldsMatch0(input struct {
	FirstField  string
	SecondField string
}) bool {
	return input.FirstField == input.SecondField
}

func fieldsMatch1(input Type1) bool {
	return input.FirstField == input.SecondField
}

func Test(t *testing.T) {
	v1 := Type1{FirstField: "Hello", SecondField: "World"}
	v2 := Type2{FirstField: "Hello", SecondField: "Hello"}

	// This would be a compilation error
	// v1, v2 = v2, v1

	// This would be a compilation error
	// if v1 != v2 {
	// t.Error("Values differ: ", v1, " vs ", v2)
	// }

	// Note that we can pass both v1 and v2 to a function which is expecting nothing but a structure containing those two fields.
	if actual, expected := fieldsMatch0(v1), false; actual != expected {
		t.Error("For ", v1, " got ", actual, " but expected ", expected)
	}
	if actual, expected := fieldsMatch0(v2), true; actual != expected {
		t.Error("For ", v1, " got ", actual, " but expected ", expected)
	}

	// But this is also illegal, since fieldsMatch0 expects exactly two fields FirstField and SecondField
	// v3 := Type3{FirstField: "Hello", SecondField: "Hello", ThirdField: "Something else"}
	// if actual, expected := fieldsMatch0(v3), true; actual != expected {
	// 	t.Error("For ", v3, " got ", actual, " but expected ", expected)
	// }
}
