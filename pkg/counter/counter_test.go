package counter

import (
	"testing"
)

type CounterThingy struct {
	message string
	click   func() int
	current func() int
}

func CreateCounter() CounterThingy {
	magic := 0
	return CounterThingy{
		message: "Hello World",
		click: func() int {
			magic++
			return magic
		},
		current: func() int {
			return magic
		},
	}
}

func Test1(t *testing.T) {
	counter1 := CreateCounter()
	counter2 := CreateCounter()

	if expected, actual := "Hello World", counter1.message; expected != actual {
		t.Error("Expected ", expected, " but got message", actual)
	}

	sequence := []struct {
		expected int
		actual   int
		memo     string
	}{
		{1, counter1.click(), "counter1"},
		{2, counter1.click(), "counter1"},
		{3, counter1.click(), "counter1"},
		{3, counter1.current(), "counter1"},
		{1, counter2.click(), "counter2"},
		{2, counter2.click(), "counter2"},
		{4, counter1.click(), "counter1"},
		{5, counter1.click(), "counter1"},
		{5, counter1.current(), "counter1"},
		{3, counter2.click(), "counter2"},
	}

	for _, step := range sequence {
		if step.expected != step.actual {
			t.Error(step.memo, ": expected ", step.expected, " but got ", step.actual)
		}
	}
}
